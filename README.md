# Infrastructure as code playground

A learning journey into infrastructure as code using terraform, packer, ansible and AWS.

These examples are stored at https://bitbucket.org/romule/iacplayground/overview

Clone it here: git@bitbucket.org:romule/iacplayground.git

Once you've cloned this repository you can get a specific version as:

    git checkout <version>
Be aware that this will put you in detatched HEAD state, but that doesn't matter unless you want to push changes back to the repository.

AWS keys are not contained in the repository.  A template terraform.tfvars file is, but you'll need to add key values to this file locally.

## v1 - Single VM in AWS
git tag: v1

example.tf declares the aws provider, and specifies a single resource (a .

The AWS region and base ami (image) are stored as variables in the variables.tf file.  The AMI id is region specific.

Access key and secret key are stored in terraform.tfvars file you'll need to get these from [AWS console](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances) and add locally.

To run this, first initialise terraform, which will install the AWS provider plugin

    $ terraform init

Then run the terraform plan (type 'yes' when prompted)

    $ terraform apply
    
Checking the [AWS console](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances) shows our new running VM.

Sadly, we can't connect to this instance, even via the console.  That's our  mission in part 2.

## v2 - Connect to our AWS VM using SSH
git tag: v2

Now we have a VM running in the cloud.  It's not running any services, and we can't connect to it.

I'm not going to lie to you, this was one of the most faff filled things I've ever tried to do, took me hours.

TL;DR; To SSH to an instance you need that instance to have a key pair, and be in a security group which exposes port 22.

Creating an instance in the console walks you through these steps.  Even then you have to call ssh passing in a private key. (I'm using a folder local private key which will never be used for anything else).

So, take a look at example.tf on the v2 tag, especially the addition of the key pair and security group.  notice that these have been referenced by the aws_instance example.

Then to try out the magic, first get the public dns of the instance (though the IP address is probably easier to type).  We can use the `terraform console` command to return the attribute we want.

    $ echo "aws_instance.example.public_dns" | terraform console

Taking this a step further we can use this to build the SSH command

    $ ssh -i iacplay-stuartleitch.private ubuntu@$(echo "aws_instance.example.public_dns" | terraform console)

Phew.  Beer o'clock.

## v3  - Old Skool Apache installation
git tag: v3

Connect to the instance we created in v2.

   $ sudo apt-get install apache2

No joy.  This box isn't allowed to talk to anything.  We're going to have to tweak the security group rules to allow outbound connections. And while we're in there, let's open up port 80 for Apache.

Check out the updated example.tf file.

Do a '`terraform apply`', note that terraform is smart and works out that only the security group has changed, and even that the change made can be done without destroying and recreating the instance.  Nice!

Reconnect to the instance with the command above, and '`ping www.google.com`'.  OK, we can talk to the world, so we're ready to install apache.

    $ sudo apt-get install apache2
    
Note down your public dns record, grab a browser, and enter the public dns name (remember http:// prefix)

Boom!  We have an apache box running in the cloud.

But wait? We just logged on and installed software! That's not cool anymore.


## v4 - The magic starts
git tag: v4

We're going to add the apache installation into our terraform recipe, so that when the instance is created Apache is already installed and up and running.

First, do a '`terraform destroy`' which clears out all the things built into AWS so far.

In terraform 'provisioners' do a lot of the work of installing and configuring software on top of existing images.  We're adding a super simple provisioner to the 'example' resource, and renaming it 'webserver'.

For this example we're using the 'remote-exec' provisioner, and passing it commands directly using the 'inline' argument.

    provisioner "remote-exec" {
      connection {
        type     = "ssh"
        user     = "ubuntu"
        private_key = "${file("iacplay-stuartleitch.private")}"
      }
      inline = [
        "sudo apt-get update",
        "sudo apt-get -y install apache2",
        ]
    }

now give it a '`terraform apply`', wait a bit and when terraform completes its work grab the dns name:

    $ echo "aws_instance.webserver.public_dns" | terraform console
Pop that into a browser and confirm that we've got an apache server up and running.  Good progress!

## v5 - Harden and configure apache
git tag: v5

We're now going to do a bit of apache configuration, but before we do that a little tidy-up.

The 'inline' argument for the remote-exec provisioner is fine for a minor tweak, but if we want to do real config we can separate the script out into a separate file called 'provision_webserver.sh'.  A benefit of thsi is that we get propoer syntax highlighting when editing that file, plus it's good separation of concerns!

Next, you may have noticed that the 'connection' section of the provisioner referenced the private key file.  The public key contents were copied into the 'key-pair' reseource definition.  This has been changed to pull the values in from the file.

### Switch to RHEL
So far our AWS instances have been based on an Ubuntu 16.04 image.  Since RHEL is our prefered flavour of Linux, we're going to switch that over before we go too far down the route of distribution specific configuration.

First thing is to grab the image ID of a base RHEL 7.4 ami.  Easiest way to do that is to log in to the AWS EC2 console, and 'Launch Instance', select 'Red Hat Enterprise Linux 7.4' from Quick Start and then grab its ami id.  In this case it's `ami-bb9a6bc2`.  Update the webserver resource definition in the example.tf file.

Also, RHEL uses yum as a package manager rather than apt-get, so we need to update our provision_webserver.sh script.  And also change the user we connect to the instance with from 'ubuntu' to 'ec2-user'.

### Harden Apache
We're not really going to do a proper production strength hardening, but just an indication of how such actions may be done.

Load the apache homepage, and if we inspect the header we'll spot the Apache version '`Server:Apache/2.4.6 (Red Hat Enterprise Linux)`'  Probably best to give as few clues away as possible, so let's remove this header.

Firstly, I connected to the instance with Cyberduck and snatched httpd.conf from /etc/httpd/conf.

Adding the following lines to the end of the file hides version info.

    ServerSignature Off
    ServerTokens Prod

Now all we need to do is get this file back on to the box when we create it.  Couple of snags... First, the file provisioner is running as ec2-user and the httpd.conf file is owned by root.  So, I copy it to the ec2-user home folder, `sudo cp` it to the correct location and then `chown` it to root.  Faff... Must be a better way, probably using a `root` user connection.

End result.  Apache is running using our new httpd.conf file.

Minor refactor to remove initial apache start and avoid restart.

### Serve our own content

I've created the world's worst web page and arranged for it to be loaded to /var/www/html where it becomes the default homepage.

Do a `terraform apply` (might need a `terraform taint`) then hit the webserver.  Check the headers to confirm the Apache version is gone.  The crappy index.html will be obvious.

## v6 - Several webservers under a load balancer

## v7 - Create a custom image to pr