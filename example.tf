provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_security_group" "aws_iacplay" {
  name = "iacplay"
  description = "IAC play security group"

  # TCP access
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # HTTP access
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Full outbound access
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "iacplay_stuartleitch" {
  key_name   = "iacplay_stuartleitch"
  public_key = "${file("iacplay-stuartleitch.pub")}"
}

resource "aws_instance" "webserver" {
  ami           = "${var.aws_example_ami}"
  instance_type = "t2.micro"
  key_name      = "iacplay_stuartleitch"
  security_groups = ["${aws_security_group.aws_iacplay.name}"]
  connection {
    type     = "ssh"
    user     = "ec2-user"
    private_key = "${file("iacplay-stuartleitch.private")}"
  }
  provisioner "remote-exec" {
    script = "provision_webserver.sh"
  }
  provisioner "file" {
      source      = "httpd.conf"
      destination = "/home/ec2-user/httpd.conf.new"
  }
  provisioner "file" {
      source      = "index.html"
      destination = "/home/ec2-user/index.html"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo cp /home/ec2-user/httpd.conf.new /etc/httpd/conf/httpd.conf",
      "sudo chown root:root /etc/httpd/conf/httpd.conf",
      "sudo cp /home/ec2-user/index.html /var/www/html/index.html",
      "sudo chown root:root /var/www/html/index.html",
      "sudo apachectl start"
    ]
  }
}