variable "access_key" {}
variable "secret_key" {}

variable "region" {
  default = "eu-west-1"
}

variable "aws_example_ami" {
  default = "ami-bb9a6bc2" # Red Hat Enterprise Linux 7.4 (HVM)
  #default = "ami-8fd760f6" # Ubuntu Server 16.04 LTS (HVM), SSD Volume Type
}
