#!/usr/bin/env bash

sudo yum -y install git
sudo yum -y install httpd
sudo systemctl enable httpd.service
# sudo systemctl start httpd.service